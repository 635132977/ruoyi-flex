package com.ruoyi.mf.domain.bo;

import com.ruoyi.mf.domain.MfStudent;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 学生信息表业务对象 mf_student
 *
 * @author 数据小王子
 * @date 2024-04-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = MfStudent.class, reverseConvertGenerate = false)
public class MfStudentBo extends BaseEntity
{

    /**
     * 编号
     */
    private Long studentId;

    /**
     * 学生名称
     */
    @NotBlank(message = "学生名称不能为空")
    private String studentName;

    /**
     * 年龄
     */
    @NotNull(message = "年龄不能为空")
    private Integer studentAge;

    /**
     * 爱好（0代码 1音乐 2电影）
     */
    @NotBlank(message = "爱好（0代码 1音乐 2电影）不能为空")
    private String studentHobby;

    /**
     * 性别（1男 2女 3未知）
     */
    @NotBlank(message = "性别（1男 2女 3未知）不能为空")
    private String studentGender;

    /**
     * 状态（0正常 1停用）
     */
    @NotBlank(message = "状态（0正常 1停用）不能为空")
    private String studentStatus;

    /**
     * 生日
     */
    @NotNull(message = "生日不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date studentBirthday;


}
