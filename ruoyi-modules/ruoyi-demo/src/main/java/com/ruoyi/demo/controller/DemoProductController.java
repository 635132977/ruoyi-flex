package com.ruoyi.demo.controller;

import java.util.List;

import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.web.core.BaseController;
import com.ruoyi.common.core.core.domain.AjaxResult;
import com.ruoyi.demo.domain.DemoProduct;
import com.ruoyi.demo.service.IDemoProductService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;


/**
 * 产品树表（mb）Controller
 *
 * @author 数据小王子
 * 2023-07-11
 */
@RestController
@RequestMapping("/demo/product")
public class DemoProductController extends BaseController
{
    @Resource
    private IDemoProductService demoProductService;

    /**
     * 查询产品树表（mb）列表
     */
    @SaCheckPermission("demo:product:list")
    @GetMapping("/list")
    public AjaxResult list(DemoProduct demoProduct)
    {
        List<DemoProduct> list = demoProductService.selectDemoProductList(demoProduct);
        return success(list);
    }

    /**
     * 导出产品树表（mb）列表
     */
    @SaCheckPermission("demo:product:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, DemoProduct demoProduct)
    {
        List<DemoProduct> list = demoProductService.selectDemoProductList(demoProduct);
        ExcelUtil<DemoProduct> util = new ExcelUtil<>(DemoProduct.class);
        util.exportExcel(response, list, "产品树表（mb）数据");
    }

    /**
     * 获取产品树表（mb）详细信息
     */
    @SaCheckPermission("demo:product:query")
    @GetMapping(value = "/{productId}")
    public AjaxResult getInfo(@PathVariable("productId") Long productId)
    {
        return success(demoProductService.selectDemoProductByProductId(productId));
    }

    /**
     * 新增产品树表（mb）
     */
    @SaCheckPermission("demo:product:add")
    @PostMapping
    public AjaxResult add(@RequestBody DemoProduct demoProduct)
    {
        return toAjax(demoProductService.insertDemoProduct(demoProduct));
    }

    /**
     * 修改产品树表（mb）
     */
    @SaCheckPermission("demo:product:edit")
    @PutMapping
    public AjaxResult edit(@RequestBody DemoProduct demoProduct)
    {
        return toAjax(demoProductService.updateDemoProduct(demoProduct));
    }

    /**
     * 删除产品树表（mb）
     */
    @SaCheckPermission("demo:product:remove")
    @DeleteMapping("/{productIds}")
    public AjaxResult remove(@PathVariable Long[] productIds)
    {
        return toAjax(demoProductService.deleteDemoProductByProductIds(productIds));
    }
}
