package com.ruoyi.mf.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.excel.annotation.ExcelDictFormat;
import com.ruoyi.common.excel.convert.ExcelDictConvert;
import lombok.Data;
import java.io.Serial;
import java.io.Serializable;
import lombok.NoArgsConstructor;

/**
 * 产品树导入视图对象 mf_product
 *
 * @author 数据小王子
 * @date 2024-04-12
 */

@Data
@NoArgsConstructor
public class MfProductImportVo implements Serializable
{

    @Serial
    private static final long serialVersionUID = 1L;

     /** 产品编号 */
    @ExcelProperty(value = "产品编号")
    private Long productId;

     /** 上级编号 */
    @ExcelProperty(value = "上级编号")
    private Long parentId;

     /** 产品名称 */
    @ExcelProperty(value = "产品名称")
    private String productName;

     /** 显示顺序 */
    @ExcelProperty(value = "显示顺序")
    private Integer orderNum;

     /** 产品状态（0正常 1停用） */
    @ExcelProperty(value = "产品状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_student_status")
    private String status;

     /** 逻辑删除标志（0代表存在 1代表删除） */
    @ExcelProperty(value = "逻辑删除标志（0代表存在 1代表删除）")
    private Integer delFlag;


}
